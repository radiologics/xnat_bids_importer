package com.radiologics.filesystems.services;


import com.radiologics.filesystems.exceptions.*;
import com.radiologics.filesystems.model.auto.FilesystemConfig;
import com.radiologics.filesystems.model.entity.FilesystemObjectInfo;
import org.apache.commons.lang3.StringUtils;
import org.nrg.xdat.model.CatEntryI;
import org.nrg.xnat.utils.CatalogUtils;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.io.*;
import java.nio.file.*;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.*;

public class LocalFilesystemService implements FilesystemService {
    String separator = File.separator;

    @Override
    public void updateProjectArchiver(String project, @Nullable Long archiverId) throws InvalidEntityException {
        // Do nothing
    }

    @Override
    public boolean servesConfig(FilesystemConfig config) {
        return false;
    }

    @Override
    public boolean supportsUrl(String url, @Nullable String project, boolean shouldExist) {
        return false;
    }

    @Override
    public boolean supportsUrl(String url, @Nullable String project, boolean shouldExist, boolean shouldBeWritable) {
        return false;
    }

    @Nullable
    @Override
    public FilesystemObjectInfo assertSupportsUrl(String url, @Nullable String project, boolean shouldExist)
            throws InvalidFilesystemOperationException {
        return assertSupportsUrl(url, project, shouldExist, false);
    }

    @Nullable
    @Override
    public FilesystemObjectInfo assertSupportsUrl(String url, @Nullable String project, boolean shouldExist,
                                                  boolean shouldBeWritable)
            throws InvalidFilesystemOperationException {
        throw new InvalidFilesystemOperationException(this, "supportsUrl");
    }

    @Override
    @Nonnull
    public InputStream getInputStream(String inputPath, @Nullable String project) throws RemoteApiException {
        try {
            return new FileInputStream(inputPath);
        } catch (FileNotFoundException e) {
            throw new RemoteApiException(e);
        }
    }

    @Override
    @Nonnull
    public File pullFile(String url, String destinationPath, @Nullable String project) {
        return new File(destinationPath);
    }

    @Override
    public void pushFile(File file, String url, @Nullable String project, boolean firstPush)
            throws InvalidFilesystemOperationException {
        throw new InvalidFilesystemOperationException(this, "pushFile");
    }

    @Override
    @Nullable
    public Object initiatePullResourceFiles(@Nonnull final String catPath,
                                            @Nullable final String catDestPath,
                                            @Nullable String project,
                                            @Nonnull final List<CatEntryI> entriesToPull) {
        // Files are local, just fake it
        return new Object();
    }

    @Override
    public double pollPullResource(@Nonnull Object progressTracker) {
        return 100;
    }

    @Override
    @Nonnull
    public String makeUriFromLocal(String absoluteLocalPath, @Nullable String project)
            throws InvalidFilesystemOperationException {
        throw new InvalidFilesystemOperationException(this, "makeUriFromLocal");
    }

    @Override
    @Nonnull
    public List<String> listAllFiles(@Nullable String inputPath, @Nullable String project) {
        inputPath = StringUtils.defaultIfBlank(inputPath, "");
        final List<String> files = new ArrayList<>();
        try {
            Files.walkFileTree(Paths.get(inputPath), new SimpleFileVisitor<Path>() {
                @Override
                public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) {
                    files.add(file.toAbsolutePath().toString());
                    return FileVisitResult.CONTINUE;
                }

                @Override
                public FileVisitResult visitFileFailed(Path file, IOException e) {
                    // Skip dirs that can't be traversed
                    return FileVisitResult.CONTINUE;
                }

                @Override
                public FileVisitResult postVisitDirectory(Path dir, IOException e) {
                    if (e == null) {
                        // Have directories end with '/'
                        files.add(dir.toAbsolutePath().toString() + separator);
                    }
                    return FileVisitResult.CONTINUE;
                }
            });
        } catch (IOException e) {
            throw new AssertionError("Files.walkFileTree shouldn't throw IOException since we modified " +
                    "SimpleFileVisitor not to do so");
        }
        return files;
    }

    @Override
    public void deleteFile(String url, @Nullable String project) throws RemoteApiException {
        try {
            Files.delete(Paths.get(url));
        } catch (IOException e) {
            throw new RemoteApiException(e);
        }
    }

    @Override
    @Nonnull
    public CatalogUtils.CatalogEntryAttributes getMetadata(String url, String catalogPath, @Nullable String project) {
        File file = new File(url);
        return new CatalogUtils.CatalogEntryAttributes(Paths.get(catalogPath).relativize(file.toPath()).toString(),
                file.getName(), file.length(), new Date(file.lastModified()), CatalogUtils.getHash(file));
    }
}
