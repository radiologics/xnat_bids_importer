package com.radiologics.bids.connection;

import com.google.common.collect.Maps;
import com.radiologics.bids.services.RemoteRESTService;
import org.apache.commons.codec.binary.Base64;
import org.nrg.xdat.bean.XnatImagesessiondataBean;
import org.nrg.xdat.bean.XnatProjectdataBean;
import org.nrg.xdat.bean.XnatSubjectdataBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.*;
import org.springframework.http.client.SimpleClientHttpRequestFactory;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.io.File;
import java.util.Map;

/**
 * The Class RemoteConnectionManager.
 *
 * @author Mohana Ramaratnam
 */
@Service
public class RemoteConnectionManager {
    private final RemoteRESTService _remoteRESTService;
    private static final Map<String, String> sessionCache = Maps.newHashMap();

    @Autowired
    public RemoteConnectionManager(final RemoteRESTService remoteRESTService) {
        _remoteRESTService = remoteRESTService;
    }

    /**
     * Gets the auth headers.
     *
     * @param connection    the connection
     * @param useJSESSIONID the use jsessionid
     * @param refreshCache  the refresh cache
     * @return the auth headers
     */
    public static HttpHeaders GetAuthHeaders(RemoteConnection connection, boolean useJSESSIONID, boolean refreshCache) {
        final HttpHeaders headers = new HttpHeaders();
        String JSESSIONID = null;
        if (refreshCache) {
            JSESSIONID = getJsessionId(connection, true);
        }
        if (useJSESSIONID) {
            if (JSESSIONID == null) {
                JSESSIONID = getJsessionId(connection);
            }
            if (JSESSIONID != null && JSESSIONID.length() > 0) {
                headers.add(HttpHeaders.COOKIE, "JSESSIONID=" + JSESSIONID);
                return headers;
            }
        }
        headers.add("Authorization", "Basic " + getBase64Credentials(connection));
        return headers;
    }

    /**
     * Gets the auth headers.
     *
     * @param connection    the connection
     * @param useJSESSIONID the use jsessionid
     * @return the http headers
     */
    public static HttpHeaders GetAuthHeaders(RemoteConnection connection, boolean useJSESSIONID) {
        // We won't refresh the JSESSSIONID cache unless we're told to
        return GetAuthHeaders(connection, useJSESSIONID, false);
    }

    /**
     * Gets the auth headers (sends credentials.  does not use the cache version).
     *
     * @param connection the connection
     * @return the http headers
     */
    public static HttpHeaders GetAuthHeaders(RemoteConnection connection) {
        // For safety, let's not use JSESSIONID unless we're told to.
        return GetAuthHeaders(connection, false, false);
    }

    /**
     * Gets the jsession id (option to refresh the cache).
     *
     * @param connection   the connection
     * @param refreshCache refresh the cache, or used cached version?
     * @return the jsession id
     */
    private synchronized static String getJsessionId(RemoteConnection connection, boolean refreshCache) {
        final String cacheKey = connectionToCacheKey(connection);
        if (!refreshCache && sessionCache.containsKey(cacheKey)) {
            return sessionCache.get(cacheKey);
        }
        final HttpEntity<?> httpEntity = new HttpEntity<Object>(GetAuthHeaders(connection, false));
        final SimpleClientHttpRequestFactory requestFactory = new SimpleClientHttpRequestFactory();
        final RestTemplate restTemplate = new RestTemplate(requestFactory);
        final ResponseEntity<String> response = restTemplate.exchange(connection.getUrl() + "/data/JSESSIONID", HttpMethod.GET, httpEntity, String.class);
        if (response.getStatusCode().equals(HttpStatus.OK)) {
            final String responseBody = response.getBody();
            if (responseBody == null || responseBody.contains("Change Password for")) {
                return null;
            }
            if (responseBody.length() > 0) {
                sessionCache.put(cacheKey, responseBody);
            }
            return response.getBody();
        }
        return null;
    }

    /**
     * Gets the jsession id (uses cached version if available (does not refresh the cache)).
     *
     * @param connection the connection
     * @return the jsession id
     */
    private static String getJsessionId(RemoteConnection connection) {
        return getJsessionId(connection, false);
    }

    /**
     * Connection to cache key.
     *
     * @param connection the connection
     * @return the string
     */
    private static String connectionToCacheKey(RemoteConnection connection) {
        return connection.getUrl();
    }

    /**
     * Gets the base64 credentials.
     *
     * @param conn the conn
     * @return the base64 credentials
     */
    private static String getBase64Credentials(RemoteConnection conn) {
        final String plainCreds;
        plainCreds = conn.getUsername() + ":" + conn.getPassword();
        final byte[] plainCredsBytes = plainCreds.getBytes();
        final byte[] base64CredsBytes = Base64.encodeBase64(plainCredsBytes);
        final String base64Creds = new String(base64CredsBytes);
        return base64Creds;
    }


    /**
     * Gets the remote REST service.
     *
     * @return The remote REST service.
     */
    public RemoteRESTService getRemoteRESTService() {
        return _remoteRESTService;
    }

    /**
     * Check access to URI
     *
     * @param connection    the connection
     * @param uri           the uri
     * @param project       the project
     * @return T/F accessible
     * @throws RuntimeException the exception
     */
    public boolean checkAccess(RemoteConnection connection, String uri, String project) throws RuntimeException {
        try {
            RemoteConnectionResponse response = _remoteRESTService.checkAccess(connection, uri, project);
            return Boolean.parseBoolean(response.getResponseBody());
        } catch (Exception e) {
            return false;
        }
    }

    /**
     * Import subject.
     *
     * @param connection the connection
     * @param subject    the subject
     * @return the remote connection response
     * @throws RuntimeException the exception
     */
    public RemoteConnectionResponse importSubject(RemoteConnection connection, XnatSubjectdataBean subject) throws RuntimeException {
        return _remoteRESTService.importSubject(connection, subject);
    }

    /**
     * Import project.
     *
     * @param connection the connection
     * @param project    the project
     * @return the remote connection response
     * @throws RuntimeException the exception
     */
    public RemoteConnectionResponse importProject(RemoteConnection connection, XnatProjectdataBean project) throws RuntimeException {
        return _remoteRESTService.importProject(connection, project);
    }

    /**
     * Import project resource.
     *
     * @param connection    the connection
     * @param projectId     the project id
     * @param resourceLabel the resource label
     * @param payload       A zip file or a Map of URLs
     * @return the remote connection response
     * @throws RuntimeException the exception
     */
    public RemoteConnectionResponse importProjectResource(RemoteConnection connection, String projectId,
                                                          String resourceLabel, Object payload) throws RuntimeException {
        if (payload instanceof File) {
            return _remoteRESTService.importProjectResource(connection, projectId, resourceLabel, (File) payload);
        } else if (payload instanceof String) {
            return _remoteRESTService.importProjectResource(connection, projectId, resourceLabel, (String) payload);
        } else {
            throw new RuntimeException("Unable to import payload of class " + payload.getClass());
        }
    }

    /**
     * Import Resource.
     *
     * @param connection    the connection
     * @param url_postfix   the well formed URL after host
     * @param payload       payload
     * @return the remote connection response
     * @throws RuntimeException the exception
     */
    public RemoteConnectionResponse importResource(RemoteConnection connection, String url_postfix,
                                                   Object payload) throws RuntimeException {
        if (payload instanceof File) {
            return _remoteRESTService.importResource(connection, url_postfix, (File) payload);
        } else if (payload instanceof String) {
            return _remoteRESTService.importResource(connection, url_postfix, (String) payload);
        } else {
            throw new RuntimeException("Unable to import payload of class " + payload.getClass());
        }
    }

    /**
     * Import MRSession resource.
     *
     * @param connection    the connection
     * @param exp           the experiment
     * @return the remote connection response
     * @throws RuntimeException the exception
     */
    public RemoteConnectionResponse importImageSession(RemoteConnection connection, XnatImagesessiondataBean exp) throws RuntimeException {
        return _remoteRESTService.importImageSession(connection, exp);
    }


    /**
     * Gets the result.
     *
     * @param connection the connection
     * @param uri        the uri
     * @return the result
     * @throws RuntimeException the exception
     */
    public RemoteConnectionResponse getResult(RemoteConnection connection, String uri) throws RuntimeException {
        return _remoteRESTService.getResult(connection, uri);
    }
}
