package com.radiologics.bids.dataimport;

import com.radiologics.bids.observer.ImportObserver;
import com.radiologics.bids.utils.BIDSConstants;
import com.radiologics.bids.utils.BIDSFileUtils;
import com.radiologics.filesystems.services.FilesystemService;
import org.apache.commons.lang3.StringUtils;
import org.nrg.xdat.bean.XnatMrscandataBean;

import java.util.*;
import java.util.function.BiFunction;
import java.util.stream.Collectors;

/**
 * @author Mohana Ramaratnam
 */
public class ScanExtractor {

    private final BIDSFileUtils _utils;
    private final FilesystemService _filesystemService;
    private final ImportObserver _observer;
    private final String _sesDir;
    private final List<String> _sesFilesAndDirs;
    private final Map<String, Properties> _propertiesMap;
    private final List<String> _subFiles;
    private final List<String> _sesDerivFilesAndDirs;
    private final List<String> _projectResources;
    private final Hashtable<String, Integer> counter = new Hashtable<>();


    public ScanExtractor(FilesystemService filesystemService, BIDSFileUtils utils,
                         ImportObserver observer,
                         String sesDir,
                         List<String> sesFilesAndDirs,
                         Map<String, Properties> propertiesMap,
                         List<String> subFiles,
                         List<String> sesDerivFilesAndDirs,
                         List<String> projectResources) {
        _filesystemService = filesystemService;
        _utils = utils;
        _observer = observer;
        _sesDir = sesDir;
        _sesFilesAndDirs = sesFilesAndDirs;
        _propertiesMap = propertiesMap;
        _subFiles = subFiles;
        _sesDerivFilesAndDirs = sesDerivFilesAndDirs;
        _projectResources = projectResources;
    }

    public Hashtable<XnatMrscandataBean, Hashtable<String, List<String>>> getScans() {

        final Hashtable<XnatMrscandataBean, Hashtable<String, List<String>>> scanFileHash = new Hashtable<>();

        // As per Lauren Wallace
        //The scan number is the BIDS folder (anat, func, dwi, etc) plus a simple counter from first to last file in the folder,
        //the scan type is the BIDS type element (last thing before the file extension),
        //and the series description is a more complete version of the BIDS file name including run number and additional info
        // See also, the BIDS spec https://bids.neuroimaging.io/bids_spec.pdf

        final List<String> datatypeDirs = _sesFilesAndDirs.stream().filter(_utils::isDir).collect(Collectors.toList());
        if (datatypeDirs.size() == 0) {
            return scanFileHash;
        }

        boolean hasDerivatives = _sesDerivFilesAndDirs != null && _sesDerivFilesAndDirs.size() > 0;

        for (String sessionDatatypeDir : datatypeDirs) {
            // The only subdirectories of a session can be data-type dirs (anat, func, dwi, beh, etc).
            String dataType = _utils.stripParentDirs(sessionDatatypeDir);

            // Collect relevant files
            final List<String> sesDatatypeFiles = _sesFilesAndDirs.stream().filter(
                    f -> _utils.startsWithDir(f, sessionDatatypeDir) && _utils.isFile(f))
                    .collect(Collectors.toList());

            // Iterate over scans (a.k.a., nifti files)
            // We handle non-scan dirs (e.g., beh) within XnatImporter > importMrSession
            List<String> niftiFiles = sesDatatypeFiles.stream().filter(_utils::isNifti).collect(Collectors.toList());
            for (String nifti : niftiFiles) {
                // Each nifti file creates a scan of type dataType
                Hashtable<String, List<String>> scanFilesByCategory = new Hashtable<>();
                scanFilesByCategory.put(BIDSConstants.NIFTI_RESOURCE, Collections.singletonList(nifti));

                // Parse info from BIDS filename
                String niftiFileName = _utils.stripParentDirs(nifti);
                String niftiRelativePath = nifti.replaceFirst(_sesDir, "");
                String niftiNoExt = niftiFileName;
                int extInd = niftiFileName.indexOf(".");
                if (extInd != -1) {
                    niftiNoExt = niftiFileName.substring(0, extInd);
                }
                Hashtable<String, String> parsedTokens = parseTokensInFileBaseName(niftiFileName);
                String scanType = parsedTokens.get(BIDSConstants.MODALITY_LABEL);
                String scanSeriesDescription = getSeriesDescription(parsedTokens);

                // Make scan bean
                XnatMrscandataBean mrScan = new XnatMrscandataBean();
                mrScan.setId(niftiNoExt);
                mrScan.setType(scanType);
                mrScan.setSeriesDescription(scanSeriesDescription);
                mrScan.setModality(scanType);
                mrScan.setQuality("usable"); // Set default, which can be overridden by Quality json param
                if (parsedTokens.contains(BIDSConstants.ACQ_PREFIX)) {
                    mrScan.setParameters_acqtype(parsedTokens.get(BIDSConstants.ACQ_PREFIX));
                }
                ScanParams scanParams = new ScanParams(_observer, _filesystemService, mrScan);

                // Collect related files (events/physio/stim, bval/bvec, etc) for upload
                List<String> bidsScanFiles = getAccompanyingFiles(niftiNoExt, parsedTokens, sesDatatypeFiles);

                // Parse params from jsons: start project, then subject, then session
                // (ORDER MATTERS in jsonFiles because we want to override params with those from the "closest" json)
                // Add scan-level json to upload list (bidsscanFiles)
                List<String> jsonFiles = getCommonJSONs(parsedTokens, sesDatatypeFiles, bidsScanFiles);
                List<String> scanJsonFiles = findFileInList(sesDatatypeFiles,
                        niftiNoExt + BIDSConstants.JSON_EXT,
                        _utils::fileEndsWith);
                jsonFiles.addAll(scanJsonFiles);
                for (String json : jsonFiles) {
                    scanParams.populate(json);
                }
                bidsScanFiles.addAll(scanJsonFiles); // only want to upload the scan-level json
                scanFilesByCategory.put(BIDSConstants.BIDS_RESOURCE, bidsScanFiles);

                // Add properties from scan tsv after jsons, this takes precedence
                if (_propertiesMap != null && _propertiesMap.containsKey(niftiRelativePath)) {
                    scanParams.populate(_propertiesMap.get(niftiRelativePath));
                }

                // Add derivatives
                if (hasDerivatives) {
                    //All files in this directory are resources files of the scan
                    List<String> derivatives = _sesDerivFilesAndDirs.stream().filter(
                            f -> _utils.containsDir(f, scanType + _filesystemService.separator)
                    ).collect(Collectors.toList());
                    scanFilesByCategory.put(BIDSConstants.DERIVATIVES_RESOURCE, derivatives);
                }

                addToScanFileHash(scanFileHash, mrScan, scanFilesByCategory);
            }

        }
        return scanFileHash;
    }

    private synchronized void addToScanFileHash(Hashtable<XnatMrscandataBean, Hashtable<String, List<String>>> scanFileHash,
                                           XnatMrscandataBean mrScan, Hashtable<String, List<String>> scanFilesByCategory) {
        scanFileHash.put(mrScan, scanFilesByCategory);
    }

    private String makeBidsClause(Hashtable<String, String> parsedTokens, String prefix) {
        return (parsedTokens.containsKey(prefix)) ? prefix + parsedTokens.get(prefix) + "_" : "";
    }

    private List<String> findFileInList(List<String> list, final String filestring,
                                          BiFunction<String, String, Boolean> matcher) {
        return list.stream().filter(f -> matcher.apply(f, filestring)).collect(Collectors.toList());
    }

    private void addAllJsonMatches(List<String> jsonFiles, String fileName, List<String> sesDatatypeFiles,
                                   List<String> resourcesToUpload) {
        //Go global to local, ORDER MATTERS
        //project wide, then within subject (if subject!=session), then session files
        jsonFiles.addAll(findFileInList(_projectResources, fileName, _utils::fileEndsWith));
        if (_subFiles != null) jsonFiles.addAll(findFileInList(_subFiles, fileName, _utils::fileEndsWith));
        List<String> scanLevel = findFileInList(sesDatatypeFiles, fileName, _utils::fileEndsWith);
        jsonFiles.addAll(scanLevel);
        resourcesToUpload.addAll(scanLevel); //we want to upload these
    }

    private List<String> getCommonJSONs(Hashtable<String, String> parsedTokens, List<String> sesDatatypeFiles,
                                        List<String> resourcesToUpload) {
        //Go global to local, ORDER MATTERS
        List<String> jsonFiles = new ArrayList<>();
        String modality = parsedTokens.get(BIDSConstants.MODALITY_LABEL);

        //Just modality json
        String fileName = modality + BIDSConstants.JSON_EXT;
        addAllJsonMatches(jsonFiles, fileName, sesDatatypeFiles, resourcesToUpload);

        if (parsedTokens.containsKey(BIDSConstants.TASK_PREFIX)) {
            //Task + modality json
            fileName = makeBidsClause(parsedTokens, BIDSConstants.TASK_PREFIX) + fileName;
            addAllJsonMatches(jsonFiles, fileName, sesDatatypeFiles, resourcesToUpload);

            //Task + acq + run + modality json
            String fileNamePlus = makeBidsClause(parsedTokens, BIDSConstants.TASK_PREFIX);
            fileNamePlus += makeBidsClause(parsedTokens, BIDSConstants.ACQ_PREFIX);
            fileNamePlus += makeBidsClause(parsedTokens, BIDSConstants.RUN_PREFIX);
            fileNamePlus += modality + BIDSConstants.JSON_EXT;
            if (!fileNamePlus.equals(fileName)) {
                addAllJsonMatches(jsonFiles, fileNamePlus, sesDatatypeFiles, resourcesToUpload);
            }
        }
        return jsonFiles;
    }

    private String getSeriesDescription(Hashtable<String, String> parsedTokens) {
        String seriesDescription = "";
        seriesDescription += makeBidsClause(parsedTokens, BIDSConstants.TASK_PREFIX);
        seriesDescription += makeBidsClause(parsedTokens, BIDSConstants.ACQ_PREFIX);
        seriesDescription += makeBidsClause(parsedTokens, BIDSConstants.REC_PREFIX);
        seriesDescription += makeBidsClause(parsedTokens, BIDSConstants.RUN_PREFIX);
        seriesDescription += makeBidsClause(parsedTokens, BIDSConstants.ECHO_PREFIX);
        seriesDescription += makeBidsClause(parsedTokens, BIDSConstants.MOD_PREFIX);

        if (parsedTokens.containsKey(BIDSConstants.MODALITY_LABEL)) {
            seriesDescription += parsedTokens.get(BIDSConstants.MODALITY_LABEL);
        }

        //Replace trailing underscore
        return seriesDescription.replaceAll("_$", "");
    }

    private boolean addToParsedTokensMap(Hashtable<String, String> parsedTokens, String prefix, String fileNameToken) {
        boolean matched = false;
        if (fileNameToken.startsWith(prefix)) {
            parsedTokens.put(prefix, fileNameToken.substring(prefix.length()));
            matched = true;
        }
        return matched;
    }

    private Hashtable<String, String> parseTokensInFileBaseName(String fileName) {
        //sub-<participant_label>[_ses-<session_label>]_task-<task_label>[_acq-<label>][_rec-<label>][_run-<index>][_echo-<index>]_bold.nii[.gz]
        Hashtable<String, String> parsedTokens = new Hashtable<>();
        String[] tokens = fileName.split("\\.");
        String baseFileName = tokens[0];
        String[] fileNameTokens = baseFileName.split("_");
        List<String> prefixes = Arrays.asList(BIDSConstants.SUBJECT_PREFIX,
                BIDSConstants.SUBJECT_PREFIX,
                BIDSConstants.SESSION_PREFIX,
                BIDSConstants.TASK_PREFIX,
                BIDSConstants.ACQ_PREFIX,
                BIDSConstants.REC_PREFIX,
                BIDSConstants.RUN_PREFIX,
                BIDSConstants.ECHO_PREFIX,
                BIDSConstants.MOD_PREFIX);
        for (String fileNameToken : fileNameTokens) {
            boolean matched = false;
            int i = 0;
            while (!matched && i < prefixes.size()) {
                // Go through all the prefixes looking for a match
                matched = addToParsedTokensMap(parsedTokens, prefixes.get(i++), fileNameToken);
            }
            if (!matched) {
                parsedTokens.put(BIDSConstants.MODALITY_LABEL, fileNameToken);
            }
        }
        return parsedTokens;
    }

    private List<String> addMiscFilesByPrefix(final String filenamePrefix, List<String> sesDatatypeFiles) {
        return sesDatatypeFiles.stream().filter(s -> _utils.fileMatchesLessExtension(s, filenamePrefix) &&
                !_utils.isNifti(s) && !_utils.isJson(s))
                .collect(Collectors.toList());
    }

    private List<String> getAccompanyingFiles(String niftiFileBaseName, Hashtable<String, String> parsedTokens,
                                              List<String> sesDatatypeFiles) {
        //This would grab, e.g., bvals and bvecs
        List<String> accompanyingFiles = addMiscFilesByPrefix(niftiFileBaseName, sesDatatypeFiles);

        //Take care of other ways of naming a file
        //Strip modality from filename
        if (parsedTokens.get(BIDSConstants.MODALITY_LABEL) != null) {
            int modalityIndex = niftiFileBaseName.lastIndexOf(parsedTokens.get(BIDSConstants.MODALITY_LABEL));
            if (modalityIndex != -1) {
                niftiFileBaseName = niftiFileBaseName.substring(0, modalityIndex).replaceAll("_$","");
            }
        }
        final String rootName = niftiFileBaseName;
        accompanyingFiles.addAll(sesDatatypeFiles.stream().filter(s ->
                        _utils.fileMatchesLessExtension(s, rootName + "_events") ||
                        _utils.fileMatchesLessExtension(s, rootName + "_physio") ||
                        _utils.fileMatchesLessExtension(s, rootName + "_stim")
                ).collect(Collectors.toList()));

        //Unclear what this is doing - which files are named like this? Same logic is in getCommonJSONs
        //sub-<participant_label>[_ses-<session_label>]_task-<task_label>[_acq-<label>][_run-<index>]
        String fileName = BIDSConstants.SUBJECT_PREFIX + parsedTokens.get(BIDSConstants.SUBJECT_PREFIX) + "_";
        fileName += makeBidsClause(parsedTokens, BIDSConstants.SESSION_PREFIX);
        String taskClause = makeBidsClause(parsedTokens, BIDSConstants.TASK_PREFIX);
        fileName += taskClause;
        String taskFileName = fileName;
        if (StringUtils.isNotBlank(taskClause)) {
            fileName += makeBidsClause(parsedTokens, BIDSConstants.ACQ_PREFIX);
            fileName += makeBidsClause(parsedTokens, BIDSConstants.RUN_PREFIX);
        }
        fileName = fileName.replaceAll("_$", "");
        taskFileName = taskFileName.replaceAll("_$", "");
        if (!fileName.equals(niftiFileBaseName)) {
            accompanyingFiles.addAll(addMiscFilesByPrefix(fileName, sesDatatypeFiles));
        }
        if (!taskFileName.equals(niftiFileBaseName) && !taskFileName.equals(fileName)) {
            accompanyingFiles.addAll(addMiscFilesByPrefix(taskFileName, sesDatatypeFiles));
        }
        //End unclear what this is doing

        return accompanyingFiles;
    }
}
