package com.radiologics.bids.dataimport;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.radiologics.bids.observer.ImportObserver;
import com.radiologics.bids.utils.BIDSConstants;
import com.radiologics.filesystems.services.FilesystemService;
import org.apache.commons.lang3.StringUtils;
import org.nrg.xdat.bean.XnatAddfieldBean;
import org.nrg.xdat.bean.XnatMrscandataBean;

import java.io.InputStream;
import java.util.Iterator;
import java.util.Properties;

public class ScanParams extends XnatBeanParams {

    private XnatMrscandataBean _mrScan;
    private FilesystemService _filesystemService;


    public ScanParams(ImportObserver observer, FilesystemService filesystemService, XnatMrscandataBean mrScan) {
        super(observer);
        _filesystemService = filesystemService;
        _mrScan = mrScan;
    }

    public void populate(Properties properties) {
        addParamsFromProperties(properties);
    }

    public void populate(String jsonFile) {
        if (jsonFile == null) {
            return;
        }
        ObjectMapper objectMapper = new ObjectMapper();
        JsonNode rootNode;
        try(InputStream is = _filesystemService.getInputStream(jsonFile, _mrScan.getProject())) {
            rootNode = objectMapper.readTree(is);
            Iterator<String> fieldNamesIterator = rootNode.fieldNames();
            while (fieldNamesIterator.hasNext()) {
                String fieldName = fieldNamesIterator.next();
                JsonNode field = rootNode.get(fieldName);
                if (field == null) {
                    continue;
                }
                try {
                    addParamCmd(fieldName, jsonNodeTextToString(field));
                } catch (Exception e) {
                    observer.error(e.getMessage(), e);
                }
            }
        } catch (Exception e) {
            observer.error(e.getMessage(), e);
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    void addParamCmd(String fieldName, String fieldValue) throws Exception {
        addParam(fieldName, fieldValue, _mrScan,
                BIDSConstants.MRSCAN_FIELD_MAP, BIDSConstants.MRSCAN_TRANSFORM_MAP);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    void addParamAsAdditional(String key, String value) {
        //Achieves, for example:
        XnatAddfieldBean addField = new XnatAddfieldBean();
        addField.setName(key);
        addField.setAddfield(value);
        _mrScan.addParameters_addparam(addField);
    }

    private String jsonNodeTextToString(JsonNode node) {
        String nodeToStr;
        if (node.isArray()) {
            StringBuilder arrStr = new StringBuilder("[");
            for (JsonNode n : node) {
                arrStr.append(n.asText()).append(",");
            }
            arrStr.append("]");
            nodeToStr = arrStr.toString().replaceAll(",]", "]");
        } else {
            nodeToStr = StringUtils.defaultIfBlank(node.asText(), node.toString());
        }
        return nodeToStr;
    }
}
