/*
 * xnat-template: org.nrg.xnat.plugins.template.plugin.XnatTemplatePlugin
 * XNAT http://www.xnat.org
 * Copyright (c) 2017, Washington University School of Medicine
 * All Rights Reserved
 *
 * Released under the Simplified BSD.
 */

package com.radiologics.bids.importer;

import com.radiologics.bids.connection.RemoteConnection;
import com.radiologics.bids.connection.RemoteConnectionHandler;
import com.radiologics.bids.connection.RemoteConnectionManager;
import com.radiologics.bids.dataimport.BIDSRootFolder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import picocli.CommandLine;
import picocli.CommandLine.Command;
import picocli.CommandLine.Option;
import picocli.CommandLine.ParameterException;


@Service
@Command(name = "BidsDataImporter",
        description = "\nBidsDataImporter: imports BIDS data into XNAT. Please refer to " +
                "https://bids.neuroimaging.io/bids_spec.pdf for information about the BIDS specification. " +
                "BidsDataImporter is compatible with BIDS v1.1.1.\n\nOptions marked with a '*' are required.\n",
        mixinStandardHelpOptions = true, version = "1.1", sortOptions = false,
        requiredOptionMarker = '*', abbreviateSynopsis = true, optionListHeading = "OPTIONS:\n")
public class BidsDataImporter {

    private final RemoteConnectionManager _manager;

    @Option(names = {"-t", "--host"}, description = "XNAT host URL\n", required = true)
    private String xnatHost;

    @Option(names = {"-u", "--user"}, description = "XNAT username\n", required = true)
    private String xnatUser;

    @Option(names = {"-p", "--password"}, description = "XNAT password\n", required = true)
    private String xnatPass;

    @Option(names = {"-l", "--logdir"}, description = "Path to writable local directory for logs\n", required = true)
    private String logBaseDir;

    @Option(names = {"-b", "--bids"}, description = "Path to BIDS data root folder " +
            "(if on S3, provide s3 URL, e.g., s3://my-bucket/my-root). This folder may be a BIDS project folder, " +
            "or a folder containing multiple BIDS project folders\n", required = true)
    private String bidsRoot;

    @Option(names = {"-i", "--pid"}, description = "Project ID (will add to existing XNAT project), " +
            "only applicable if root folder is a BIDS project folder\n")
    private String projectId = null;


    @Option(names = {"-a", "--accesskey"}, description = "AWS S3 access key. If not provided, you must have credentials " +
            "in environment variables AWS_ACCESS_KEY_ID and AWS_SECRET_ACCESS_KEY on this machine or use the default " +
            "credential profiles file, typically located at ~/.aws/credentials (location can vary per platform), " +
            "used with AWS CLI.\n")
    private String awsAccessKey = null;

    @Option(names = {"-s", "--secretkey"}, description = "AWS S3 secret key. If not provided, you must have credentials " +
            "in environment variables AWS_ACCESS_KEY_ID and AWS_SECRET_ACCESS_KEY on this machine or use the default " +
            "credential profiles file, typically located at ~/.aws/credentials (location can vary per platform), " +
            "used with AWS CLI.\n")
    private String awsSecretKey = null;

    @Autowired
    public BidsDataImporter(final RemoteConnectionManager manager) {
        _manager = manager;
    }


    public void importData(String[] args) throws Exception {
        CommandLine cmd = new CommandLine(this);
        try {
            cmd.parse(args);
            if (cmd.isUsageHelpRequested()) {
                cmd.usage(System.out);
                return;
            } else if (cmd.isVersionHelpRequested()) {
                cmd.printVersionHelp(System.out);
                return;
            }
        } catch (ParameterException e) {
            cmd.usage(System.out);
            throw e;
        }

        try {
            // Open XNAT connection
            RemoteConnectionHandler remoteConnectionHandler = RemoteConnectionHandler.GetInstance(xnatHost,
                    xnatUser, xnatPass);
            RemoteConnection connection = remoteConnectionHandler.getRemoteConnection();

            // Do import
            BIDSRootFolder bidsRootFolder = new BIDSRootFolder(_manager, connection, logBaseDir,
                    bidsRoot, projectId, awsAccessKey, awsSecretKey);
            bidsRootFolder.importData();
        } finally {
            System.out.println("Log files in  " + logBaseDir);
        }
    }
}
