package com.radiologics.bids.services;

import com.radiologics.bids.connection.RemoteConnection;
import com.radiologics.bids.connection.RemoteConnectionResponse;
import org.nrg.xdat.bean.XnatImagesessiondataBean;
import org.nrg.xdat.bean.XnatProjectdataBean;
import org.nrg.xdat.bean.XnatSubjectdataBean;

import java.io.File;
import java.util.Map;

/**
 * @author Mohana Ramaratnam
 */
public interface RemoteRESTService {
    public RemoteConnectionResponse checkAccess(RemoteConnection connection, String uri, String project) throws RuntimeException;

    public RemoteConnectionResponse importProject(RemoteConnection connection, XnatProjectdataBean project) throws RuntimeException;

    public RemoteConnectionResponse importProjectResource(RemoteConnection connection, String projectId,
                                                          String resourceLabel, File zipFile) throws RuntimeException;

    public RemoteConnectionResponse importProjectResource(RemoteConnection connection, String projectId,
                                                          String resourceLabel, String urlJson) throws RuntimeException;

    public RemoteConnectionResponse importSubject(RemoteConnection connection, XnatSubjectdataBean subject) throws RuntimeException;

    public RemoteConnectionResponse importImageSession(RemoteConnection connection, XnatImagesessiondataBean experiment) throws RuntimeException;

    public RemoteConnectionResponse importResource(RemoteConnection connection, String url_postfix, File zipFile) throws RuntimeException;

    public RemoteConnectionResponse importResource(RemoteConnection connection, String url_postfix, String urlJson) throws RuntimeException;


    public RemoteConnectionResponse getResult(RemoteConnection connection, String uri) throws RuntimeException;

}
