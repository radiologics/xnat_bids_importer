package com.radiologics.bids.utils;

import java.util.regex.Pattern;

public class BIDSFileUtils {

    // NOTE: directories must end in trailing separator

    private String separator;

    private final Pattern pathPrefixPattern;
    private final Pattern pathSuffixPattern;
    private final Pattern trailingSeparator;
    private final Pattern filenamePattern;


    //E.g., on unix /any/path/sub-
    private final Pattern subPrefixRegex;
    //E.g., on unix, matches /any/path/sub-[anything not a path separator]/
    private final Pattern subDirRegex;
    //E.g., on unix /any/path/sub-[anything not a path separator]/ses-
    private final Pattern sesPrefixRegex;
    //E.g., on unix, matches /any/path/sub-[anything not a path separator]/ses-[anything not a path separator]/
    private final Pattern sesDirRegex;

    public BIDSFileUtils(String separator) {
        this.separator = separator;

        trailingSeparator = Pattern.compile(separator + "$");
        pathPrefixPattern =  Pattern.compile("^.*" + separator);
        pathSuffixPattern =  Pattern.compile(separator + ".*$");
        filenamePattern = Pattern.compile("[^" + separator + "]*$");

        String subRegexBase = "^.*" + separator + BIDSConstants.SUBJECT_PREFIX;
        subPrefixRegex = Pattern.compile(subRegexBase);
        subDirRegex = Pattern.compile(subRegexBase + "[^" + separator + "]*" + separator);

        String sesRegexBase = subDirRegex + BIDSConstants.SESSION_PREFIX;
        sesPrefixRegex = Pattern.compile(sesRegexBase);
        sesDirRegex = Pattern.compile(sesRegexBase + "[^" + separator + "]*" + separator);
    }

    public boolean fileEndsWith(String s, String name) {
        return s.endsWith(separator + name);
    }
    public boolean dirEndsWith(String s, String name) {
        return s.endsWith(separator + name + separator);
    }
    public boolean isDir(String s) {
        return s.endsWith(separator);
    }
    public boolean isFile(String s) {
        return !isDir(s);
    }
    public boolean isNifti(String s) {
        return s.endsWith(BIDSConstants.NII_EXT) || s.endsWith(BIDSConstants.NII_GZ_EXT);
    }
    public boolean isJson(String s) {
        return s.endsWith(BIDSConstants.JSON_EXT);
    }

    private String combineDirs(String parent, String... parentSubdirs) {
        StringBuilder dirPath = new StringBuilder(parent);
        for (String d : parentSubdirs) {
            dirPath.append(d);
        }
        return dirPath.toString();
    }
    public boolean startsWithDir(String s, String parent, String... parentSubdirs) {
        return s.startsWith(combineDirs(parent, parentSubdirs));
    }
    public boolean containsDir(String s, String dir, String... subdirs) {
        return s.contains(combineDirs(dir, subdirs));
    }

    public boolean representsSubjectDir(String s) {
        //Match entire string
        return subDirRegex.matcher(s).matches();
    }
    public boolean withinSubjectDir(String s) {
        //Match prefix
        return subDirRegex.matcher(s).lookingAt();
    }
    public String extractSubjectLabel(String bidsFolder) {
        //Strip subject dir path & prefix, then strip separator and anything after
        return pathSuffixPattern.matcher(subPrefixRegex.matcher(bidsFolder).replaceAll(""))
                .replaceAll("");
    }

    public boolean representsSessionDir(String s) {
        //Match entire string
        return sesDirRegex.matcher(s).matches();
    }
    public boolean withinSessionDir(String s) {
        //Match prefix
        return sesDirRegex.matcher(s).lookingAt();
    }
    public String extractSessionLabel(String bidsFolder) {
        //Strip session dir path & prefix, then strip separator and anything after
        return pathSuffixPattern.matcher(sesPrefixRegex.matcher(bidsFolder).replaceAll(""))
                .replaceAll("");
    }

    public boolean fileMatchesLessExtension(String s, String prefix) {
        String rootFileName = s;
        int lastIndexOfDot = s.lastIndexOf(".");
        if (lastIndexOfDot != -1) {
            rootFileName = s.substring(0, lastIndexOfDot);
        }
        return fileEndsWith(rootFileName, prefix);
    }
    public String stripFilename(String s) {
        //greedily replace anything that is not a path separator from end of string
        return filenamePattern.matcher(s).replaceAll("");
    }
    public String stripParentDirs(String s) {
        //strip trailing separator (if it's there), then greedily replace everything up to and ending with separator
        return pathPrefixPattern.matcher(trailingSeparator.matcher(s).replaceAll(""))
                .replaceAll("");
    }

    public String getBIDSSessionDir(String label) {
        return BIDSConstants.SESSION_PREFIX + label + separator;
    }
    public String getBIDSSessionTsv(String subjectDir) {
        String subjectBidsName = stripParentDirs(subjectDir);
        return subjectDir +
                subjectBidsName + BIDSConstants.SESSIONS_TSV_SUFFIX;
    }
    public String getBIDSScanTsv(String sessionDir) {
        String sessionBidsName = stripParentDirs(sessionDir);
        if (!representsSubjectDir(sessionDir)) {
            String subjectDir = trailingSeparator.matcher(sessionDir).replaceAll("")
                    .replaceAll(sessionBidsName, "");
            String subjectBidsName = stripParentDirs(subjectDir);
            sessionBidsName = subjectBidsName + "_" + sessionBidsName;
        }
        return sessionDir +
                sessionBidsName + BIDSConstants.SCANS_TSV_SUFFIX;
    }


    //Static methods
    public static String clearPrefix(String label, String prefix) {
        String cleanedLabel = label;
        if (label.startsWith(prefix)) {
            cleanedLabel = label.replaceFirst("^" + prefix, "");
        }
        return cleanedLabel;
    }
}
