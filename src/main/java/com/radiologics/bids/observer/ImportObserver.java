package com.radiologics.bids.observer;

import com.radiologics.bids.manifest.ImportManifest;
import com.radiologics.bids.utils.BIDSConstants;
import org.apache.commons.lang3.StringUtils;
import org.nrg.xdat.bean.XnatSubjectdataBean;
import org.nrg.xdat.bean.base.BaseElement;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.regex.Pattern;

/**
 * @author Mohana Ramaratnam
 */
public class ImportObserver implements Observer {

    private final String importFileSuffix = "_import";
    private final String projectId;
    private final String baseLogPath;
    private final File logFile;
    private final File errFile;
    private BufferedWriter bWriter;
    private FileWriter fw;
    private Date syncStartTime;
    private boolean success = true;

    private ImportManifest importManifest;
    private Map<String, String> subjectXNATMap = new HashMap<>();
    private Map<String, String> experimentXNATMap = new HashMap<>();

    public ImportObserver(String projectId, String baseLogPath) {
        this.projectId = projectId;
        this.baseLogPath = baseLogPath.replaceAll(File.separator + "*$", File.separator); // Already created

        String baseStr = getProjectLogDir() + projectId + importFileSuffix;
        logFile = new File(baseStr + ".log");
        errFile = new File(baseStr + ".err");

        logFile.getParentFile().mkdirs();
        try {
            fw = new FileWriter(logFile);
            bWriter = new BufferedWriter(fw);
        } catch (IOException e) {
            System.err.println("Unable to write to file " + e.getMessage());
        }

    }

    public boolean isSuccess() {
        return success;
    }

    public void update(Observable observable, Object item) {
        synchronized (logFile) {
            try {
                Date now = new Date();
                bWriter.write("------------------- BEGIN " + now + " ------------------------");
                bWriter.newLine();
                bWriter.write(item.toString());
                bWriter.newLine();
                bWriter.flush();
            } catch (IOException e) {
                System.err.println("An error occurred writing the log sync file " + logFile.getAbsolutePath() + ": " +
                        e.getMessage());
                e.printStackTrace();
            }
        }
    }

    public void close() {
        try {
            Date now = new Date();
            bWriter.write("------------------- END " + now + " --------------------------");
            bWriter.newLine();
            bWriter.close();
            fw.close();
        } catch (IOException e) {
            System.err.println("Unable to close file buffers");
            e.printStackTrace();
        }
    }

    public void debug(String message) {
        debug(message, null);
    }

    public void debug(String message, Exception e) {
        synchronized (errFile) {
            try (BufferedWriter bw = new BufferedWriter(new FileWriter(errFile))) {
                bw.write(message);
                bw.newLine();
                bw.write(StringUtils.join(e.getStackTrace(), BIDSConstants.NEWLINE));
                bw.newLine();
                bw.flush();
            } catch (Exception ex) {
                System.err.println("An error occurred writing the sync errlog " + errFile.getAbsolutePath() + ": " +
                        ex.getMessage());
                ex.printStackTrace();
            }
        }
    }

    public void error(String message) {
        error(message, null);
    }
    public void error(String message, Exception e) {
        // Also print to stderr
        System.err.println(message);
        e.printStackTrace();
        // Print to log
        debug(message, e);

        if (success) {
            synchronized (this) {
                success = false;
            }
        }
    }

    private Map<String, String> getMapForType(String type) {
        Map<String, String> map;
        if (type == null) type = "";
        switch (type) {
            case XnatSubjectdataBean.SCHEMA_ELEMENT_NAME:
                map = subjectXNATMap;
                break;
            default:
                map = experimentXNATMap;
                break;
        }
        return map;
    }

    public synchronized void saveXnatId(String label, String id, String type) {
        Map<String, String> map = getMapForType(type);
        map.put(label, id);
    }

    public synchronized String getXnatId(String label, String type) {
        return getMapForType(type).getOrDefault(label, null);
    }

    public void beginSync(Date syncStartTime) {
        this.syncStartTime = syncStartTime;
        ImportManifest projectSyncManifest = new ImportManifest(projectId);
        projectSyncManifest.setSync_start_time(syncStartTime);
        importManifest = projectSyncManifest;
    }

    public synchronized void updateManifest(String message) {
        updateManifest(message, false);
    }
    public synchronized void updateManifest(String message, boolean markIssue) {
        importManifest.appendMessage(message);
        if (markIssue) importManifest.markIssue();
    }

    public void endSync(boolean save) {
        Date now = new Date();
        importManifest.setSync_end_time(now);
        if (importManifest.shouldNotify())
            importManifest.informUser();
        File syncInfoFilePath = new File(getProjectLogDir() + projectId + importFileSuffix + ".html");
        importManifest.syncInfoToFile(syncInfoFilePath);
        //Clean up the cache path contents
        if (importManifest.wasSyncSuccessfull()) cleanUp();
    }

    private void cleanUp() {
        File folder = new File(getProjectLogDir());
        if (folder.exists()) rmdir(folder);
    }

    private void rmdir(File folder) {
        File[] list;
        // check if folder file is a real folder
        if (!folder.isDirectory() || (list = folder.listFiles()) == null) {
            return;
        }
        Pattern importFile = Pattern.compile(".*" + importFileSuffix + "\\.[^.]*$");
        for (File tmpF : list) {
            if (tmpF.isDirectory()) {
                rmdir(tmpF);
            }
            if (!importFile.matcher(tmpF.getAbsolutePath()).matches()) {
                tmpF.delete();
            }
        }
    }

    public String saveFailedUpload(BaseElement element, String elementLabel) {
        importManifest.markIssue();
        File projectXMLFilePath = new File(getProjectLogFailureDir() + elementLabel + "_failed.xml");
        try(FileWriter writer = new FileWriter(projectXMLFilePath)) {
            element.toXML(writer, true);
        } catch (IOException e) {
            System.err.println("Unable to write " + projectXMLFilePath.getAbsolutePath());
        }
        return projectXMLFilePath.getAbsolutePath();
    }

    private String timeToPath(Date d) {
        SimpleDateFormat ft =
                new SimpleDateFormat("yyyy.MM.dd.hh.mm.ss");
        if (d == null)
            d = new Date();
        return ft.format(d);
    }

    public String getProjectLogDir() {
        String syncPath = baseLogPath;
        String timestampFolder = timeToPath(syncStartTime);
        syncPath += "BIDSImport-" + projectId + "_" + timestampFolder + File.separator;
        return syncPath;
    }

    public String getProjectLogFailureDir() {
        String fPath = getProjectLogDir() + "FAILURES" + File.separator;
        File f = new File(fPath);
        if (!f.exists()) f.mkdirs();
        return fPath;
    }


}
